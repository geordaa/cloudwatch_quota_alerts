# cloudwatch_quota_alerts

Creating metric and dimension alerts from cloudwatch quotas

## Background

You can create Amazon CloudWatch alarms on the Service Quotas console to notify you when you're close to a quota value threshold. Setting an alarm can help you know if you need to request a quota increase.

## References

- [ ] [Cloudwatch Quotas](https://docs.aws.amazon.com/servicequotas/latest/userguide/configure-cloudwatch.html) 
- [ ] [Terraform Resource](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_metric_alarm) 
