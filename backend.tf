terraform {  
    backend "s3" {
        bucket  = "geordaa-terraform-backend-store"
        encrypt = true
        key     = "cloudwatch_quota_alerts/terraform.tfstate"
        region  = "eu-west-2"
    }
}