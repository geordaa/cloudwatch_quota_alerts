provider "aws" {
  region = "eu-west-2"
}

resource "aws_cloudwatch_metric_alarm" "quota-running-on-demand-standard-instances" {
    actions_enabled           = true
    alarm_actions             = var.alarm_actions
    alarm_description         = "High amount of running on demand standard instances"
    alarm_name                = "geordaa-quota-running-on-demand-standard-instances"
    comparison_operator       = "GreaterThanOrEqualToThreshold"
    datapoints_to_alarm       = 10
    evaluation_periods        = 10
    insufficient_data_actions = []
    ok_actions                = []
    threshold                 = 60
    treat_missing_data        = "missing"
    metric_query {
        id          = "usage_data"
        return_data = false
        metric {
            dimensions  = {
                "Class"    = "Standard/OnDemand"
                "Resource" = "vCPU"
                "Service"  = "EC2"
                "Type"     = "Resource"
            }
            metric_name = "ResourceCount"
            namespace   = "AWS/Usage"
            period      = 60
            stat        = "Maximum"
        }
    }
    metric_query {
        expression  = "(usage_data/SERVICE_QUOTA(usage_data))*100"
        id          = "pct_utilization"
        label       = "% Utilization"
        return_data = true
    }
}

resource "aws_cloudwatch_metric_alarm" "quota-running-spot-standard-instances" {
    actions_enabled           = true
    alarm_actions             = var.alarm_actions
    alarm_description         = "High amount of running spot standard instances"
    alarm_name                = "geordaa-quota-running-spot-standard-instances"
    comparison_operator       = "GreaterThanOrEqualToThreshold"
    datapoints_to_alarm       = 10
    evaluation_periods        = 10
    insufficient_data_actions = []
    ok_actions                = []
    threshold                 = 60
    treat_missing_data        = "missing"
    metric_query {
        id          = "usage_data"
        return_data = false
        metric {
            dimensions  = {
                "Class"    = "Standard/Spot"
                "Resource" = "vCPU"
                "Service"  = "EC2"
                "Type"     = "Resource"
            }
            metric_name = "ResourceCount"
            namespace   = "AWS/Usage"
            period      = 60
            stat        = "Maximum"
        }
    }
    metric_query {
        expression  = "(usage_data/SERVICE_QUOTA(usage_data))*100"
        id          = "pct_utilization"
        label       = "% Utilization"
        return_data = true
    }
}

resource "aws_cloudwatch_metric_alarm" "quota-high-ALB-per-Region" {
    actions_enabled     = true
    alarm_actions       = var.alarm_actions
    alarm_description   = "High amount of ALBs in the region detected"
    alarm_name          = "geordaa-quota-high-ALB-per-Region"
    comparison_operator = "GreaterThanOrEqualToThreshold"
    datapoints_to_alarm = 10
    dimensions          = {
        "Class"    = "None"
        "Resource" = "ApplicationLoadBalancersPerRegion"
        "Service"  = "Elastic Load Balancing"
        "Type"     = "Resource"
    }
    evaluation_periods  = 10
    metric_name         = "ResourceCount"
    namespace           = "AWS/Usage"
    period              = 60
    statistic           = "Maximum"
    tags_all            = {}
    threshold           = 400
    treat_missing_data  = "missing"
    unit                = "None"
}

resource "aws_cloudwatch_metric_alarm" "quota-high-auto-scaling-groups-per-Region" {
    actions_enabled     = true
    alarm_actions       = var.alarm_actions
    alarm_description   = "High amount of Auto Scaling Groups in the region detected"
    alarm_name          = "geordaa-quota-high-auto-scaling-groups-per-Region"
    comparison_operator = "GreaterThanOrEqualToThreshold"
    datapoints_to_alarm = 10
    dimensions          = {
        "Class"    = "None"
        "Resource" = "NumberOfAutoScalingGroup"
        "Service"  = "AutoScaling"
        "Type"     = "Resource"
    }
    evaluation_periods  = 10
    metric_name         = "ResourceCount"
    namespace           = "AWS/Usage"
    period              = 60
    statistic           = "Maximum"
    tags_all            = {}
    threshold           = 160
    treat_missing_data  = "missing"
    unit                = "None"
}

resource "aws_cloudwatch_metric_alarm" "quota-high-listeners-per-ALB" {
    actions_enabled     = true
    alarm_actions       = var.alarm_actions
    alarm_description   = "High number of Listeners per Application Load Balancer detected"
    alarm_name          = "geordaa-quota-high-listeners-per-ALB"
    comparison_operator = "GreaterThanOrEqualToThreshold"
    datapoints_to_alarm = 10
    dimensions          = {
        "Class"    = "None"
        "Resource" = "ListenersPerApplicationLoadBalancer"
        "Service"  = "Elastic Load Balancing"
        "Type"     = "Resource"
    }
    evaluation_periods  = 10
    metric_name         = "ResourceCount"
    namespace           = "AWS/Usage"
    period              = 60
    statistic           = "Maximum"
    tags_all            = {}
    threshold           = 40
    treat_missing_data  = "missing"
    unit                = "None"
}

resource "aws_cloudwatch_metric_alarm" "quota-high-target-groups-per-Region" {
    actions_enabled     = true
    alarm_actions       = var.alarm_actions
    alarm_description   = "High amount of Target Groups in the region detected"
    alarm_name          = "geordaa-quota-high-target-groups-per-Region"
    comparison_operator = "GreaterThanOrEqualToThreshold"
    datapoints_to_alarm = 10
    dimensions          = {
        "Class"    = "None"
        "Resource" = "TargetGroupsPerRegion"
        "Service"  = "Elastic Load Balancing"
        "Type"     = "Resource"
    }
    evaluation_periods  = 10
    metric_name         = "ResourceCount"
    namespace           = "AWS/Usage"
    period              = 60
    statistic           = "Maximum"
    tags_all            = {}
    threshold           = 2500
    treat_missing_data  = "missing"
    unit                = "None"
}