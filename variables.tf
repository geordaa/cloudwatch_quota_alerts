variable "alarm_actions" {
  description = "SNS Topic to send alerts to"
  type        = list
  default     = ["arn:aws:sns:eu-west-2:028677494693:geordaa-cloudwatch-alerts"]
}

